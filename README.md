## 引入文件说明
### 项目中不使用ramda  引入一个文件
```html
  <script type="text/javascript" src="../dist/route.all.min.js"></script>
```
## 使用ramda 引入三个文件
```html
<script src="../lib/ramda.min.js"></script>
<script src="../lib/jueji.js"></script>
<script src="../js/route.js"></script>
```
# 浏览器路由匹配
`内含函数流，中断，异步功能`
```javascript
var s = J.stream;
var ds = J.dostream;
var dofn = route.route([
	s(route.nameP("/hello"),domlog)
	,s(route.nameP("/"),domlog)
	,s(route.paramP("/:a/:b"),domlog)
	,s(route.nameP("/hello"),domlog)
	,s(route.startP("/yibu"),R.tap(loadState),yibutest,domlog)
	,s(diyP,domlog)
	,domlog404
]);
dofn("/hello");
```
## 自动监控浏览器#(hash)发生变化
```javascript
//监控hash变化 获取URL执行dofn
ds(null,route.hashMilldam,dofn);
```
## 自动监控H5路由
```javascript
//利用H5 api改变URL 放入浏览器历史记录
//需要引入文件 /js/h5routeMilldam.js
ds(null,h5r.h5routeMilldam,dofn);
```
## 详见demo [在线demo](http://sandbox.runjs.cn/show/mg3ch4e4)

